//==========================THEORY===============================================

/*Наведіть кілька прикладів, коли доречно використовувати в коді конструкцію try...catch.

Конструкція try....catch  дуже зручна в нашому програмуванні, щоб проводити певні маніпуляції з формами заповнень (видавати помилку, якщо особа не заповнила усі поля) та в роботі з DOM (Коли нам потрібно відображати помилку на екрані для користувачів).
Також на великих проектах з тяжкими задачами вона допомагає виділяти помилку в проблемній таскі, та продовжувати роботу далі, щоб не зупинятися на одному місці. 

*/
// ===========================PRACTICE============================================
import {books} from './arr.js';



function createBookList(){
    const root = document.getElementById('root');
    const list = document.createElement('ul');


    books.forEach((obj) => {
     let item = document.createElement('li');
     
     try{
        item.textContent = `${obj.author} - ${obj.name} - Price: ${obj.price}`;
        if(!obj.price){
            throw new Error('You didn\'t pointed price!!!');
        }if(!obj.name){
            throw new Error('You didn\'t pointed name!!!')
        }if(!obj.author){
            throw new Error('You didn\'t pointed author!!!')
        }
     }catch(error){
        console.log(error);
        item.style.color = "red";
        // item.textContent = "";
        // Або
        item.textContent = error;

     }

     list.append(item);
    })
    root.append(list);
}

createBookList();